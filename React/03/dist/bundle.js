'use strict';

var _ReactDOM = ReactDOM,
    render = _ReactDOM.render;


var stl = {
	backgroundColor: 'orange',
	color: 'white',
	fontFamily: 'Verdana'
};

var elmt = React.createElement(
	'div',
	null,
	React.createElement(
		'h1',
		{ id: 'titre',
			className: 'header',
			style: stl },
		'Bonjour!!'
	),
	React.createElement(
		'p',
		null,
		'ceci est un texte'
	)
);

render(elmt, document.getElementById('root'));
