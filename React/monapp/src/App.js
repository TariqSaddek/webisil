import React, { Component } from 'react';
import './style/bootstrap.min.css';
import './style/App.css';
import {textVal} from "./textVal";
import marked from "marked";

class App extends Component {
  state={
    text: ""
  }
  handleEdite=(e)=>{
    const t=e.target.value;
    const newState={
      text: t
    };
    this.setState(newState);
  }

  renderText = (text)=>{
       let t=marked(text,{sanitize: true});
        return { __html: t};
  }

  render() {
    const titre=this.props.titre;
    return (
      <div className="container">
        <div className="titre">
          <h1>{titre}</h1>
          <p>Par: {this.props.nom}</p>
        </div>
        <div className="row">
          <div className="col-sm-6">
            <textarea rows="20" className="form-control" 
            onChange={(e)=> this.handleEdite(e)}></textarea>
          </div>
          <div className="col-sm-6" dangerouslySetInnerHTML={this.renderText(this.state.text)}>
          </div>
        </div>
      </div>
    );
  }
}

export default App;
