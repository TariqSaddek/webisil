var express = require('express');
var app = express();

app.get('/', function(req, res) {
    res.setHeader('Content-Type', 'text/plain');
    res.write('Accueil');
});

app.get('/contact', function(req, res) {
    res.setHeader('Content-Type', 'text/plain');
    res.write('Nous contscter');
});

app.get('/cours/web/nodejs', function(req, res) {
    res.setHeader('Content-Type', 'text/plain');
    res.write('Node.js is greate');
});

app.use(function(req, res, next){
    res.setHeader('Content-Type', 'text/plain');
    res.status(404).send('Page introuvable !');
});

app.listen(3000);