import React, { Component } from 'react';
import Navigation from './Components/Navigation/Navigation';
import SigninForm from './Components/SigninForm/SigninForm';
import RegisterForm from './Components/RegisterForm/RegisterForm';
import './App.css';

class App extends Component {
	constructor(){
		super();
		this.state={

			page: 'signin'

		};
	}
	componentDidMount(){
		fetch('http://localhost:3000/')
		.then(resp=>resp.json())
		.then(jsn=>console.log(jsn));
	}
	onSignin=()=>{
		let data=JSON.stringify({
					email: this.state.email,
					passwd: this.state.passwd
				});
		fetch('http://localhost:300/signin',
			{
				methode:'post',
				headers:{'content-Type': 'application/json'},
				body: data
		}).then(resp=>resp.json())
		.then(user=>{

		})
	}

	onRouteChange=(page)=>{
		this.setState({page: page});
	}

	render() {
		var content='';
		if (this.state.page==='signin') content=<SigninForm onroutechange={this.onRouteChange}/>
		else content=<RegisterForm onroutechange={this.onRouteChange}/>
	    return (
	      <div className="App">
		    <Navigation />
		    {content}
	      </div>
	    );
	}
}

export default App;
