const express=require('express');
const bodyParser=require('body-parser');
const bcrypt=require('bcrypt-nodejs');
const cors=require('cors');
const MongoClient = require('mongodb').MongoClient

const app=express();

app.use(bodyParser.json());
app.use(cors());
var db

MongoClient.connect('mongodb://localhost:27017/', { useNewUrlParser: true },(err, client) => {
  if (err) return console.log(err)
  db = client.db('nodedb') // whatever your database name is
  app.listen(3000, () => {
    console.log('listening on 3000')
  })
})

app.post('/register', (req, res) => {
  db.collection('user').save(req.body, (err, result) => {
    if (err) return console.log(err)
    console.log('saved to database')
    res.redirect('/')
  })
})

app.get('/', (req, res) => {
	 db.collection('user').find().toArray(function(err, results) {
	 	res.json(results);
	 });
});
app.post('/signin', (req, res) => {
	 db.collection('user').find().toArray(function(err, results) {
	  	let usr=results.filter((item)=>{
	  			return (item.email===req.body.email && item.passwd===req.body.passwd);
	  		});
	  	console.log(usr);
	  	if(usr.length=!0) res.json(usr);
	  	else res.json("User not found");

	});
  
});

app.post('/profile/:nom', (req, res) => {
	 db.collection('user').find({nom: req.params.nom}).toArray(function(err, results) {
	  	if(results.length=!0) res.json(results);
	  	else res.json("User not found");

	});
  
});