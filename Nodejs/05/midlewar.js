var express = require('express');
var favicon = require('serve-favicon'); // Charge le middleware de favicon
var morgan = require('morgan'); // Charge le middleware de logging

var app = express();
app.use(morgan('combined')); // Active le middleware de logging
// Indiquer que le dossier /public contient des fichiers 
//statiques (middleware chargé de base)
app.use(express.static(__dirname + '/public')); 

// Activer la favicon indiquée
//app.use(favicon(__dirname + '/public/favicon.ico')); 

// Répondre a la requete
app.use(function(req, res){ 
    res.send('Hello');
});

app.listen(3000);