function n2l(n) {
    if (isNaN(n) || n < 0 || 999 < n) {
        return "Entrez un entier (0-999)";
    }
    var u2l = ["", "un", "deux", "trois", "quatre", "cinq", "six", "sept","huit", "neuf", "dix", "onze", "douze", "treize", "quatorze", "quinze", "seize", "dix-sept", "dix-huit", "dix-neuf"],
        d2l = ["", "dix", "vingt", "trente", "quarante", "cinquante", "soixante", "soixante", "quatre-vingt", "quatre-vingt"];
    var u = n % 10,
        d = (n % 100 - u) / 10,
        c = (n % 1000 - n % 100) / 100;
    var su, sd, sc;
    if (n === 0) {
        return "zero";
    } else {
        su = u2l[u];
        if (d === 1 && u > 0) {
            sd = u2l[10 + u];
            su = "";
        } else if (d === 7 || d === 9) {
            sd = d2l[d] + "-" + u2l[10 + u];
            su = "";
        } else {
            sd = d2l[d];
        }
        if(c>1) sc=u2l[c] + "-cent";
        else if(c==1) sc="cent";
        else sc="";
        var r=sc;
        r+=sc && sd ? "-" : "";
        r+=sd;
        r+=sc && su || sd && su ? "-" : "";
        r+=su;
        return r;
    }
}
var n = prompt("Indiquez le nombre à écrire en toutes lettres (entre 0 et 999) :")
alert(n2l(parseInt(n, 10)));