function px2em(px){
	var basePx=16;
	function calc(){
		return px/basePx;
	}
	return calc;
}
var sm=px2em(12);
var md=px2em(18);
console.log("Small: ",sm());
console.log("Medium: ",md());