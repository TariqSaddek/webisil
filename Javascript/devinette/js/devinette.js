/* 
Activité : jeu de devinette
*/

// NE PAS MODIFIER OU SUPPRIMER LES LIGNES CI-DESSOUS
// COMPLETEZ LE PROGRAMME UNIQUEMENT APRES LE TODO

console.log("Bienvenue dans ce jeu de devinette !");

// Cette ligne génère aléatoirement un nombre entre 1 et 100
var solution = Math.floor(Math.random() * 100) + 1;

// Décommentez temporairement cette ligne pour mieux vérifier le programme
console.log("(La solution est " + solution + ")");

// TODO : complétez le programme
a=Number(prompt("Un nombre: "));
while(a!==solution && a!=null){
	if(a<solution) console.log("trop petit");
	else console.log("Trop grand");
	a=Number(prompt("Un nombre: "));
}
if(a!=null) console.log("Bravo!! la solution est: "+a);
